
#import <MapKit/MapKit.h>

@interface MKPolyline (EncodedString)

+ (float)decodeBytes:(const char *)bytes atPos:(NSUInteger *)idx toValue:(float *)value;
+ (MKPolyline *)polylineWithEncodedString:(NSString *)encodedString;
+ (NSMutableData *)dataWithEncodedString:(NSString *)encodedString;

@end
