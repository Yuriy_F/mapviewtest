//
//  POI.swift
//  MapViewTest
//
//  Created by Yuriy Fateykin  on 10/7/17.
//  Copyright © 2017 com.mac.yuriy. All rights reserved.
//

import UIKit
import MapKit

class POI : NSObject, MKAnnotation {
    var title: String?
    var latitude: Double
    var longitude: Double
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    init?(title: String, lat: Double, lng: Double) {
        self.title = title
        self.latitude = lat
        self.longitude = lng
    }
}
