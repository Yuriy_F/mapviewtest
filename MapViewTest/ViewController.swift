//
//  ViewController.swift
//  MapViewTest
//
//  Created by Yuriy Fateykin  on 9/19/17.
//  Copyright © 2017 com.mac.yuriy. All rights reserved.
//

import UIKit
import MapKit


class ViewController: UIViewController, MKMapViewDelegate {
    var polylineString: String! = nil


    @IBAction func getAnotation(_ sender: AnyObject) {
        // Connect all the mappoints using Poly line.
        self.mapView.addAnnotations(self.poiArray);
        
        var points: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        
        for annotation in poiArray {
            points.append(annotation.coordinate)
        }
       
        let decodedData = Data(base64Encoded: polylineString)!
        let decodedString = String(data: decodedData, encoding: .utf8)!
        
        let polyline = MKPolyline(encodedString:decodedString)!//MKPolyline(coordinates: &points, count: points.count)
        mapView.add(polyline)
        
   

    }
    
    @IBOutlet weak var mapView: MKMapView!
    var poiArray: Array<POI> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        self.zoomToRegion()
        self.getJSON()

        
        //let r = self.route
//        let sourceLocation = CLLocationCoordinate2D(latitude: 50.4501, longitude: 30.5234)
//        let destinationLocation = CLLocationCoordinate2D(latitude:  50.452914, longitude: 30.514269)
//        
//        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
//        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
//        
//        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
//        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
//        
//        let sourceAnnotation = self.createAnnatationWithTitle(title: "Center")
//        
//        if let location = sourcePlacemark.location {
//            sourceAnnotation.coordinate = location.coordinate
//        }
//        
//        let destinationAnnotation = self.createAnnatationWithTitle(title: "Volodimyr")
//        
//        if let location = destinationPlacemark.location {
//            destinationAnnotation.coordinate = location.coordinate
//        }
//        
//
//        
//        mapView.showAnnotations([sourceAnnotation, destinationAnnotation], animated: true )
//        
//        let directionRequest = MKDirectionsRequest()
//        directionRequest.source = sourceMapItem
//        directionRequest.destination = destinationMapItem
//        directionRequest.transportType = .walking
//        
//        let directions = MKDirections(request: directionRequest)
//        
//        directions.calculate {
//            (response, error) -> Void in
//            
//            guard let response = response else {
//                if let error = error {
//                    print("Error: \(error)")
//                }
//                
//                return
//            }
//            
//            
//            let route = response.routes[0]
//            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)
//            
//            let rect = route.polyline.boundingMapRect
//            self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
//        }
    }
    
    func createAnnatationWithTitle(title: String) -> MKPointAnnotation {
        let annotation = MKPointAnnotation()
        annotation.title = title
        return annotation;
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 4.0
        
        return renderer
    }
    
    func zoomToRegion() {
        let location = CLLocationCoordinate2D(latitude: 50.4501, longitude: 30.5234)
        let region = MKCoordinateRegionMakeWithDistance(location, 3000.0, 5000.0)
        
        mapView.setRegion(region, animated: true)
    }

    func getJSON() {
        let urlString = "http://demo0445830.mockable.io/route/id"
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with:url!) { (data, response, error) in
            if error != nil {
                print(error)
            } else {
                do {
                    let parsedData = try JSONSerialization.jsonObject(with: data!) as! [String:Any]
                    let currentConditions = parsedData["pois"] as! NSArray
                    
                    for dict in currentConditions as! [[String:Any]]{
                        if let title = dict["name"] as? String {
                            if let loc = dict["location"] as? [String:Any] {
                                let lat: Double = loc["lat"] as! Double
                                let lng: Double = loc["lng"] as! Double
                                
                                print(lat, lng)
                                let poiObject = POI.init(title: title, lat: lat, lng: lng)
                                self.poiArray.append(poiObject!)
                            }
                        }
                    }
                    self.polylineString = parsedData["polyline"] as! String
                    
                    //completion()
                } catch let error as NSError {
                    print(error)
                }
            }
            }.resume()
    }
}

